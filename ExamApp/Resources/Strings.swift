//
//  Strings.swift
//  ExamApp
//
//  Created by Carlos Rivero on 21/12/20.
//

import Foundation

enum Strings: String {
    
    case appName = "ExamApp"
    
    case cellIdentifier = "cell"
    
    case cardType = "card"
    
    case data = "data"
    
    case cancel = "Cancelar"
    case accept = "Aceptar"
    
    case currentLocation = "Ubicación actual:"
    case unknow = "Desconocida"
    
    case notificationsEmty = "No hay notificaciones"
    case notificationsTitle = "Notificaciones"
    case detailsTitle = "Detalles de notificación"
    
    case titleConfirm = "Eliminar Notificaciones"
    case confirmDescription = "Se eliminarán todas las notificaciones recibidas hasta ahora. \n¿Desea continuar?"
}
