//
//  BaseViewController.swift
//  ExamApp
//
//  Created by Carlos Rivero on 20/12/20.
//

import Foundation
import UIKit

class BaseViewController: UIViewController {
    
    var presenter: Presenter! {
        return nil
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupProgramView()
        setupConstraints()
        presenter.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        presenter.viewWillAppear()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        presenter.viewWillDisappear()
    }
    
    func setupProgramView() { }
    func setupConstraints() { }
}
