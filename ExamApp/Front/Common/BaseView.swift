//
//  BaseView.swift
//  ExamApp
//
//  Created by Carlos Rivero on 21/12/20.
//

import Foundation
import UIKit

protocol BaseView: class {
    func showConfirm(title: String, message: String, completion: @escaping (ConfirmResult) -> Void)
}

extension UIViewController: BaseView {
    
    func showConfirm(title: String, message: String, completion: @escaping (ConfirmResult) -> Void) {
        let confirmViewController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        confirmViewController.addAction(UIAlertAction(title: Strings.cancel.rawValue, style: .default, handler: nil))
        confirmViewController.addAction(UIAlertAction(title: Strings.accept.rawValue, style: .destructive, handler: { (action) in
            completion(.accept)
        }))
        present(confirmViewController, animated: true, completion: nil)
    }
}
