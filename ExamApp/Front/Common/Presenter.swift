//
//  Presenter.swift
//  ExamApp
//
//  Created by Carlos Rivero on 20/12/20.
//

import Foundation

protocol Presenter {
    func viewDidLoad()
    func viewWillAppear()
    func viewDidAppear()
    func viewWillDisappear()
}

extension Presenter {
    func viewDidLoad() { }
    func viewWillAppear() { }
    func viewDidAppear() { }
    func viewWillDisappear() { }
}
