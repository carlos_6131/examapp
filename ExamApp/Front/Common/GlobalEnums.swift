//
//  GlobalEnums.swift
//  ExamApp
//
//  Created by Carlos Rivero on 22/12/20.
//

import Foundation

enum NotificationType: Equatable {
    
    case card(Card)
    
    var rawValue: String {
        switch self {
            case .card(_): return Strings.cardType.rawValue
        }
    }
    
    static func ==(lhs: NotificationType, rhs: NotificationType) -> Bool {
        return lhs.rawValue == rhs.rawValue
    }
}

enum ConfirmResult {
    case cancel
    case accept
}
