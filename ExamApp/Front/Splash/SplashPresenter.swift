//
//  SplashPresenter.swift
//  ExamApp
//
//  Created by Carlos Rivero on 20/12/20.
//

import Foundation

protocol SplashView: class { }

class SplashPresenter: Presenter {
    
    private weak var splashView: SplashView?
    private let mainFlowController: MainFlowController
    
    var notificationTriggered: NotificationType? = nil

    init(view: SplashView, mainFlowController: MainFlowController) {
        self.splashView = view
        self.mainFlowController = mainFlowController
    }
    
    func viewDidLoad() {
        let notificationTriggered = self.notificationTriggered
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 1.5) { [weak self] in
            self?.mainFlowController.presentMainViewController(notificationTriggered: notificationTriggered)
        }
    }
}
