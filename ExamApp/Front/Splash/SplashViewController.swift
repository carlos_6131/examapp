//
//  SplashViewController.swift
//  ExamApp
//
//  Created by Carlos Rivero on 20/12/20.
//

import Foundation
import UIKit

class SplashViewController: BaseViewController {
    
    var splashPresenter: SplashPresenter!
    
    override var presenter: Presenter! {
        return splashPresenter
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func setupProgramView() {
        let appNameLabel = UILabel()
        appNameLabel.translatesAutoresizingMaskIntoConstraints = false
        appNameLabel.font = UIFont.systemFont(ofSize: 30.0, weight: .semibold)
        appNameLabel.text = Strings.appName.rawValue
        view.addSubview(appNameLabel)
        
        appNameLabel.centerYAnchor.constraint(equalTo: self.view.centerYAnchor).isActive = true
        appNameLabel.centerXAnchor.constraint(equalTo: self.view.centerXAnchor).isActive = true
    }
}

extension SplashViewController: SplashView { }
