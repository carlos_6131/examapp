//
//  ViewController.swift
//  ExamApp
//
//  Created by Carlos Rivero on 20/12/20.
//

import UIKit

class MainViewController: BaseViewController {

    var mainPresenter: MainPresenter!
    var cards: [Card] = []
    
    var tableView: UITableView!
    var emptyLabel: UILabel!
    
    @objc private func didSelectClearData() {
        showConfirm(title: Strings.titleConfirm.rawValue, message: Strings.confirmDescription.rawValue) { [weak self] (result) in
            switch result {
                case .accept: self?.mainPresenter.didSelectClearData()
                default: break
            }
        }
    }
    
    override var presenter: Presenter! {
        return mainPresenter
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }

    override func setupProgramView() {
        
        view.backgroundColor = .systemGroupedBackground
        navigationItem.title = Strings.appName.rawValue
        
        tableView = UITableView(frame: .zero, style: .grouped)
        tableView.translatesAutoresizingMaskIntoConstraints = false
        tableView.register(UITableViewCell.self, forCellReuseIdentifier: Strings.cellIdentifier.rawValue)
        tableView.delegate = self
        tableView.dataSource = self
        
        emptyLabel = UILabel()
        emptyLabel.textColor = .gray
        emptyLabel.font = UIFont.systemFont(ofSize: 13)
        emptyLabel.text = Strings.notificationsEmty.rawValue.uppercased()
        emptyLabel.textAlignment = .center
        emptyLabel.translatesAutoresizingMaskIntoConstraints = false
        
        let clearDataButton = UIBarButtonItem(barButtonSystemItem: .trash, target: self, action: #selector(didSelectClearData))
        navigationItem.rightBarButtonItem = clearDataButton
        
        view.addSubview(tableView)
        tableView.addSubview(emptyLabel)
    }
    
    override func setupConstraints() {
        
        tableView.topAnchor.constraint(equalTo: self.view.topAnchor).isActive = true
        tableView.leadingAnchor.constraint(equalTo: self.view.leadingAnchor).isActive = true
        tableView.trailingAnchor.constraint(equalTo: self.view.trailingAnchor).isActive = true
        tableView.bottomAnchor.constraint(equalTo: self.view.bottomAnchor).isActive = true
        
        emptyLabel.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        emptyLabel.centerYAnchor.constraint(equalTo: view.centerYAnchor).isActive = true
    }
}

extension MainViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return cards.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: Strings.cellIdentifier.rawValue, for: indexPath)
        let data = cards[indexPath.row]
        cell.textLabel?.text = data.title
        cell.imageView?.image = UIImage(named: data.thumbnail)
        cell.accessoryType = .disclosureIndicator
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let data = cards[indexPath.row]
        mainPresenter.navigateToDetailsView(card: data)
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 55.0
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 55.0
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return cards.count == 0 ? nil : Strings.notificationsTitle.rawValue
    }
}

extension MainViewController: MainView {
    
    func updateCardsAvailables(cards: [Card], animated: Bool) {
        self.cards = cards
        animated ? tableView.reloadSections([0], with: .fade) : tableView.reloadData()
        emptyLabel.isHidden = cards.count == 0 ? false : true
        navigationItem.rightBarButtonItem?.isEnabled = cards.count == 0 ? false : true
    }
}

