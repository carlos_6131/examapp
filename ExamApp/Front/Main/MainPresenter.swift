//
//  MainPresenter.swift
//  ExamApp
//
//  Created by Carlos Rivero on 20/12/20.
//

import Foundation
import RealmSwift

protocol MainView: BaseView {
    func updateCardsAvailables(cards: [Card], animated: Bool)
}

class MainPresenter: Presenter {
    
    private weak var view: MainView?
    private let detailsFlowController: DetailsFlowController
    private let dataManager: DataManager
    private let notificationPushManager: NotificationPushManager
    private let locationManager: LocationManager
    
    var notificationTriggered: NotificationType?
    var cards: [Card] = []
    
    init(view: MainView, detailsFlowController: DetailsFlowController, dataManager: DataManager, notificationPushManager: NotificationPushManager, locationManager: LocationManager) {
        self.view = view
        self.detailsFlowController = detailsFlowController
        self.dataManager = dataManager
        self.notificationPushManager = notificationPushManager
        self.locationManager = locationManager
    }
    
    func viewDidLoad() {
        dataManager.delegate = self
        if let notificationTriggeredType = notificationTriggered {
            switch notificationTriggeredType {
                case .card(let card): navigateToDetailsView(card: card)
            }
        }
    }
    
    func viewWillAppear() {
        updateMainData(animated: false)
        notificationPushManager.resetApplicationBadgeNumber()
        notificationPushManager.removeAllDeliveredNotifications()
    }
    
    func didSelectClearData() {
        dataManager.clearAll()
        notificationPushManager.resetApplicationBadgeNumber()
        notificationPushManager.removeAllDeliveredNotifications()
    }
    
    func navigateToDetailsView(card: Card) {
        locationManager.requestAuthorization()
        detailsFlowController.presentDetailsViewController(card: card)
    }
    
    private func updateMainData(animated: Bool) {
        let cards: [Card] = DataManager.instance.fetchDataAvailable()
        view?.updateCardsAvailables(cards: cards, animated: animated)
    }
}

extension MainPresenter: DataManagerDelegate {
    
    func didChangeData() {
        updateMainData(animated: true)
    }
    
    func didClearAllData() {
        view?.updateCardsAvailables(cards: [], animated: true)
    }
}
