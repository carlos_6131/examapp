//
//  MainFlowController.swift
//  ExamApp
//
//  Created by Carlos Rivero on 20/12/20.
//

import Foundation
import UIKit

class MainFlowController {
    
    private let window: UIWindow
    private var navigationController: UINavigationController?
    
    init(window: UIWindow) {
        self.window = window
    }
    
    func presentMainViewController(notificationTriggered: NotificationType?) {
        
        let mainViewController: MainViewController = MainViewController(nibName: nil, bundle: nil)
        let navigationController = UINavigationController(rootViewController: mainViewController)
        mainViewController.mainPresenter = MainPresenter(view: mainViewController, detailsFlowController: DetailsFlowController(navigationController: navigationController), dataManager: DataManager.instance, notificationPushManager: NotificationPushManager.instance, locationManager: LocationManager.instance)
        mainViewController.mainPresenter.notificationTriggered = notificationTriggered
        self.navigationController = navigationController
        window.changeRootViewController(navigationController)
    }
}
