//
//  DetailsPresenter.swift
//  ExamApp
//
//  Created by Carlos Rivero on 20/12/20.
//

import Foundation
import CoreLocation

protocol DetailsView: BaseView {
    func showCardDetails(card: Card, location: String?)
    func updateLocation(location: String)
}

class DetailsPresenter: Presenter {
    
    private weak var view: DetailsView?
    private let locationManager: LocationManager
    var card: Card?
    
    init(view: DetailsView, locationManager: LocationManager) {
        self.view = view
        self.locationManager = locationManager
        locationManager.delegate = self
    }
    
    func viewDidLoad() {
        guard let card = self.card else { return }
        showCardDetails(card: card, update: false)
    }
    
    func showCardDetails(card: Card, update: Bool) {
        self.card = update ? card : self.card
        view?.showCardDetails(card: card, location: nil)
    }
    
    func viewWillAppear() {
        locationManager.startUpdatingLocation()
    }
    
    func viewWillDisappear() {
        locationManager.stopUpdatingLocation()
    }
}

extension DetailsPresenter: LocationManagerDelegate {
    
    func didUpdateLocation(coordinate: CLLocationCoordinate2D) {
        view?.updateLocation(location: "\(coordinate.latitude), \(coordinate.longitude)")
    }
}
