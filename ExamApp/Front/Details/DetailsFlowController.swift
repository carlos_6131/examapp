//
//  DetailsFlowController.swift
//  ExamApp
//
//  Created by Carlos Rivero on 20/12/20.
//

import Foundation
import UIKit

class DetailsFlowController {
    
    private var navigationController: UINavigationController?
    
    init(navigationController: UINavigationController) {
        self.navigationController = navigationController
    }
    
    func presentDetailsViewController(card: Card) {
        let detailsViewController = DetailsViewController(nibName: nil, bundle: nil)
        detailsViewController.detailsPresenter = DetailsPresenter(view: detailsViewController, locationManager: LocationManager.instance)
        detailsViewController.detailsPresenter.card = card
        navigationController?.pushViewController(detailsViewController, animated: true)
    }
}
