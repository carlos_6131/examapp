//
//  DetailsViewController.swift
//  ExamApp
//
//  Created by Carlos Rivero on 20/12/20.
//

import Foundation
import UIKit

class DetailsViewController: BaseViewController {
    
    var titleLabel: UILabel!
    var imageDetailView: UIImageView!
    var descriptionTextView: UITextView!
    var lastLocationLabel: UILabel!
    var detailsPresenter: DetailsPresenter!
    
    override var presenter: Presenter! {
        return detailsPresenter
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationItem.title = Strings.detailsTitle.rawValue
    }
    
    override func setupProgramView() {
        view.backgroundColor = .systemGroupedBackground
        
        imageDetailView = UIImageView()
        imageDetailView.translatesAutoresizingMaskIntoConstraints = false
        imageDetailView.backgroundColor = .lightGray
        imageDetailView.clipsToBounds = true
        imageDetailView.contentMode = .scaleAspectFill
        view.addSubview(imageDetailView)
        
        titleLabel = UILabel()
        titleLabel.translatesAutoresizingMaskIntoConstraints = false
        titleLabel.font = UIFont.systemFont(ofSize: 30.0, weight: .bold)
        titleLabel.textColor = .white
        titleLabel.numberOfLines = 0
        titleLabel.shadowOffset = CGSize(width: 4.0, height: 2.0)
        titleLabel.shadowColor = .darkGray
        imageDetailView.addSubview(titleLabel)
        
        lastLocationLabel = UILabel()
        lastLocationLabel.translatesAutoresizingMaskIntoConstraints = false
        lastLocationLabel.textAlignment = .center
        lastLocationLabel.backgroundColor = .systemBlue
        lastLocationLabel.textColor = .white
        lastLocationLabel.font = UIFont.systemFont(ofSize: 14, weight: .regular)
        imageDetailView.addSubview(lastLocationLabel)
        
        descriptionTextView = UITextView()
        descriptionTextView.isUserInteractionEnabled = false
        descriptionTextView.translatesAutoresizingMaskIntoConstraints = false
        descriptionTextView.font = UIFont.systemFont(ofSize: 16.0, weight: .semibold)
        descriptionTextView.textColor = .darkGray
        view.addSubview(descriptionTextView)
    }
    
    override func setupConstraints() {
        imageDetailView.topAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.topAnchor).isActive = true
        imageDetailView.leadingAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.leadingAnchor).isActive = true
        imageDetailView.trailingAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.trailingAnchor).isActive = true
        imageDetailView.heightAnchor.constraint(equalToConstant: view.frame.width).isActive = true
        
        titleLabel.topAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.topAnchor, constant: 20.0).isActive = true
        titleLabel.leadingAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.leadingAnchor, constant: 20.0).isActive = true
        
        lastLocationLabel.bottomAnchor.constraint(equalTo: self.imageDetailView.bottomAnchor).isActive = true
        lastLocationLabel.leadingAnchor.constraint(equalTo: self.imageDetailView.leadingAnchor).isActive = true
        lastLocationLabel.trailingAnchor.constraint(equalTo: self.imageDetailView.trailingAnchor).isActive = true
        lastLocationLabel.heightAnchor.constraint(equalToConstant: 30.0).isActive = true
        
        descriptionTextView.topAnchor.constraint(equalTo: self.imageDetailView.bottomAnchor, constant: 10).isActive = true
        descriptionTextView.bottomAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.bottomAnchor, constant: -10).isActive = true
        descriptionTextView.leadingAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.leadingAnchor, constant: 10).isActive = true
        descriptionTextView.trailingAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.trailingAnchor, constant: -10).isActive = true
    }
}

extension DetailsViewController: DetailsView {
    
    func updateLocation(location: String) {
        lastLocationLabel.text = "\u{1F4CD}" + location
    }
    
    func showCardDetails(card: Card, location: String?) {
        titleLabel.text = card.title
        lastLocationLabel.text = "\(Strings.currentLocation.rawValue) \(location ?? Strings.unknow.rawValue)"
        descriptionTextView.text = card.descriptionInfo
        imageDetailView.image = UIImage(named: card.image)
    }
}
