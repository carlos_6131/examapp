//
//  Results.swift
//  ExamApp
//
//  Created by Carlos Rivero on 22/12/20.
//

import Foundation
import RealmSwift

extension Results {
    
    func toArray() -> [Element] {
        return compactMap {
            $0
        }
    }
}
