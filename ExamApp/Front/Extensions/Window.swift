//
//  Window.swift
//  ExamApp
//
//  Created by Carlos Rivero on 20/12/20.
//

import Foundation
import UIKit

extension UIWindow {
    
    func changeRootViewController(_ rootViewController: UIViewController, duration: TimeInterval = 0.65) {
        
        if self.rootViewController == nil {
            self.rootViewController = rootViewController
            return
        }
        
        guard let snapshot = snapshotView(afterScreenUpdates: true) else { return }
        
        rootViewController.view.addSubview(snapshot)
        self.rootViewController = rootViewController
        
        UIView.animate(withDuration: duration, animations: {
            snapshot.layer.opacity = 0
            snapshot.layer.transform = CATransform3DMakeScale(1.50, 1.50, 1.50)
        }, completion: { _ in
            snapshot.removeFromSuperview()
        })
    }
}
