//
//  DataManager.swift
//  ExamApp
//
//  Created by Carlos Rivero on 21/12/20.
//

import Foundation
import RealmSwift

protocol DataManagerDelegate {
    func didClearAllData()
    func didChangeData()
}

class DataManager {
    
    static var instance: DataManager = DataManager()
    var delegate: DataManagerDelegate?
    private var realm = try! Realm()
    
    func clearAll() {
        try! realm.write {
            realm.deleteAll()
            delegate?.didClearAllData()
        }
    }
    
    func persist(data: [Object]) {
        try! realm.write {
            realm.add(data, update: .modified)
            delegate?.didChangeData()
        }
    }
    
    func fetchDataAvailable<T: Object>()-> [T] {
        return realm.objects(T.self).toArray()
    }
}
