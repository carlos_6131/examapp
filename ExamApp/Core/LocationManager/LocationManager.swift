//
//  LocationManager.swift
//  ExamApp
//
//  Created by Carlos Rivero on 23/12/20.
//

import Foundation
import CoreLocation

protocol LocationManagerDelegate: class {
    func didUpdateLocation(coordinate: CLLocationCoordinate2D)
}

class LocationManager: NSObject {
    
    static var instance: LocationManager = LocationManager()
    private var locationManager: CLLocationManager = CLLocationManager()
    weak var delegate: LocationManagerDelegate?
    
    override init() {
        super.init()
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
    }
    
    func requestAuthorization() {
        locationManager.requestAlwaysAuthorization()
    }
    
    func startUpdatingLocation() {
        if CLLocationManager.locationServicesEnabled() {
            locationManager.startUpdatingLocation()
        }
    }
    
    func stopUpdatingLocation() {
        locationManager.stopUpdatingLocation()
    }
}

extension LocationManager: CLLocationManagerDelegate {
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if let currentLocation = locations.first {
            let coordinate = CLLocationCoordinate2D(latitude: currentLocation.coordinate.latitude, longitude: currentLocation.coordinate.longitude)
            delegate?.didUpdateLocation(coordinate: coordinate)
        }
    }
}
