//
//  NotificationPushManager.swift
//  ExamApp
//
//  Created by Carlos Rivero on 21/12/20.
//

import Foundation
import UserNotifications
import UIKit
import RealmSwift

class NotificationPushManager: NSObject {
    
    static var instance: NotificationPushManager = NotificationPushManager()
    private var granted: Bool? = nil
    var isGranted: Bool? {
        return granted
    }
    var test: Bool = false
    
    override init() {
        super.init()
        UNUserNotificationCenter.current().delegate = self
    }
    
    private func parseNotificationType(with userInfo: [AnyHashable : Any]) -> NotificationType? {
        let json = userInfo[Strings.data.rawValue] as! [String: Any]
        guard let card = Card(json: json) else { return nil }
        return .card(card)
    }
    
    func handlerNotification(userInfo: [AnyHashable : Any], completion: ((UNNotificationPresentationOptions) -> Void)?) {
        if let notificationType = parseNotificationType(with: userInfo) {
            switch notificationType {
                case .card(let card): DataManager.instance.persist(data: [card])
                    completion?(UNNotificationPresentationOptions.sound)
            }
        }
    }
    
    func handlerNotification(notificationType: NotificationType, completion: ((UNNotificationPresentationOptions) -> Void)?) {
        switch notificationType {
            case .card(let card): DataManager.instance.persist(data: [card])
                completion?(UNNotificationPresentationOptions.sound)
        }
    }
    
    func handlerNotification(options connectionOptions: UIScene.ConnectionOptions, completion: (NotificationType?) -> Void) {
        guard let userInfo = connectionOptions.notificationResponse?.notification.request.content.userInfo else { return }
        let notificationType = parseNotificationType(with: userInfo)
        completion(notificationType)
    }
    
    func registerForNotificationsPush() {
        UNUserNotificationCenter.current().requestAuthorization(options: [.alert, .sound, .badge]) { [unowned self] (granted, error) in
            self.granted = granted
            if granted {
                DispatchQueue.main.async {
                    UIApplication.shared.registerForRemoteNotifications()
                }
            }
        }
    }
    
    func restoreDeliveredNotifications(notificationType: NotificationType) {
        UNUserNotificationCenter.current().getDeliveredNotifications { [unowned self] (notifications) in
            let notificationsType = notifications.compactMap({ self.parseNotificationType(with: $0.request.content.userInfo) }).filter({ $0 == notificationType })
            let notificationsData = self.getNotificationData(from: notificationsType)
            DispatchQueue.main.async {
                DataManager.instance.persist(data: notificationsData)
            }
        }
    }
    
    func resetApplicationBadgeNumber() {
        UIApplication.shared.applicationIconBadgeNumber = 0
    }
    
    func removeAllDeliveredNotifications() {
        UNUserNotificationCenter.current().removeAllDeliveredNotifications()
    }
    
    private func getNotificationData(from notificationsType: [NotificationType]) -> [Object] {
        var notificationsData: [Object] = []
        for notificationType in notificationsType {
            switch notificationType {
                case .card(let card): notificationsData.append(card)
            }
        }
        return notificationsData
    }
}

extension NotificationPushManager: UNUserNotificationCenterDelegate {
        
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        NotificationPushManager.instance.handlerNotification(userInfo: notification.request.content.userInfo, completion: { presentationOption in
            completionHandler(presentationOption)
        })
    }
    
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
        if let notificationType = parseNotificationType(with: response.notification.request.content.userInfo) {
            NotificationPushManager.instance.handlerNotification(notificationType: notificationType, completion: { [weak self] _ in
                self?.navigateToCorrespondingViewController(notificationType: notificationType)
                completionHandler()
            })
        }
    }
}

extension NotificationPushManager {
    
    func navigateToCorrespondingViewController(notificationType: NotificationType) {
        if let navigationController = UIApplication.shared.windows.first(where: { $0.isKeyWindow })?.rootViewController as? UINavigationController {
            let visibleViewController = navigationController.visibleViewController
            if visibleViewController?.isKind(of: MainViewController.self) ?? false {
                let mainViewController = visibleViewController as! MainViewController
                switch notificationType {
                    case .card(let card): mainViewController.mainPresenter.navigateToDetailsView(card: card)
                }
            } else if visibleViewController?.isKind(of: DetailsViewController.self) ?? false {
                let detailsViewController = visibleViewController as! DetailsViewController
                switch notificationType {
                    case .card(let card): detailsViewController.detailsPresenter.showCardDetails(card: card, update: true)
                }
            }
        }
    }
}

