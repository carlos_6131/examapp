//
//  Card.swift
//  ExamApp
//
//  Created by Carlos Rivero on 21/12/20.
//

import Foundation
import RealmSwift

class Card: Object {
    
    @objc dynamic var id: Int = 0
    @objc dynamic var title: String = ""
    @objc dynamic var descriptionInfo: String = ""
    @objc dynamic var thumbnail: String = ""
    @objc dynamic var image: String = ""
    
    convenience init(id: Int, title: String, descriptionInfo: String, thumbnail: String, image: String) {
        self.init()
        self.id = id
        self.title = title
        self.descriptionInfo = descriptionInfo
        self.thumbnail = thumbnail
        self.image = image
    }
    
    convenience init?(json: [String: Any]) {
        self.init()
        guard let id = json["id"] as? Int,
            let title = json["title"] as? String,
            let description = json["description"] as? String,
            let thumbnail = json["thumbnail"] as? String,
            let image = json["image"] as? String else {
            return nil
        }
        self.id = id
        self.title = title
        self.descriptionInfo = description
        self.thumbnail = thumbnail
        self.image = image
    }
    
    override class func primaryKey() -> String? {
        return "id"
    }
}
