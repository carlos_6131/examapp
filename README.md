# ExamApp iOS
> ExamApp is a single application where local data persistence and remote push notifications are implemented.

## Features

- [x] PushNotification
- [x] Location Manager
- [x] Realm mobile database

## Requirements

- iOS 13.0+
- Xcode 11.0

## Installation

#### CocoaPods
You can use [CocoaPods](http://cocoapods.org/) to install dependencies

```Localize Your Folder
pod install
```

## Test Notifications
```Localize Your Folder
xcrun simctl push booted NotificationsDummy/notificationOne.apns
``` 

```Localize Your Folder
xcrun simctl push booted NotificationsDummy/notificationTwo.apns
```
